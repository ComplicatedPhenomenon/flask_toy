之前的readme 如同一个笑话大全，而且有很多碎碎念（一些小技巧[^1]，一些教训[^2]），各个部分各自独立，不存在逻辑关系

[[_TOC_]]

# ToDo list
前端
* [x] 创建`jinja` 模板, 用来托管多个独立页面
* [x] 使用`js`对数据做处理
* [ ] 在终端中运行`js`脚本[^tj]
* [x] 在一个页面上渲染多个图表[^4]
* [~] 拖拽方式生成页面反向生成CSS 样式[^5]
* [x] 异步加载页面[^async]
    * [x] 定时执行异步函数
* [x] d3 渲染多种chart[^d3api][^da1][^da2][^skillset]
    * [x] 计算坐标轴上tick的单位
    * [x] 单条line chart
    * [x] 用新数据渲染一个存在的chart, 视觉上需要连续，即只重新渲染改变的部分
        * [x] 数据对应的点
        * [x] 坐标轴的domain[^domain], 怎么改变当前的domain
        * [x] 鼠标事件，比如随着鼠标的移动显示数据点的坐标(属性随着鼠标的移动发生变化)
        * [x] 定时刷新时之前的text清除
    * [ ] d3 mouseover multi-line chart [^mo]
        * [ ] 多条线的坐标随着鼠标移动的显示
* [x] 使用流行框架[^cssframework], 实现一个完整的前端项目
    * [x] `axios`
    * [x] `vue`[^vue]
        * [x] 模板继承 + html + `vue` + 单独文件的`js` + 本地库 + 远程库 + `jinja` `vue`变量 混用
* [ ] 渲染`vue`[^render]
    * [x] table（`vue-good-table-next`）
    * [x] list（卡片，条目）
    * [x] chart
    * [x] navigator(不同页面)
    * [x] layout
    * [x] 同一个页面的导航(长页面由多个组件构成，要快速跳转到不同区域, TOC)
    * [ ] 多个view之间不能连续来回切换
    * [x] 向子组件传递数据[^pd]
    * [x] 分页，页面变化时触发函数改变`vue`变量
    * [x] 跳转到一个view（页面）时，顺带给这个页面传送参数[^dynamicroute]
    * [x] 由于一个组件适合具体格式的数据，数据需要设定schema，尽可能和具体的项目解耦
* [~] 把之前的`jinja`模板页面整合进`vue`项目

后端
* [x] 在同一个端口同时运行`fastapi`中`flask`的服务
* [x] 容器化这个项目
* [x] 与数据库交互[^database]
    * [x] `postgresql`
    * [x] 使用`redis`作缓存[^redis]
    * [x] `elasticsearch`
        * [x] common/complex DSL
* [x] WebSockets
* [x] `nginx` reverse proxy[^nrp][^nrp2](Deploying NGINX as an API Gateway)
* [x] api gateway[^apig]
* [ ] event-driven programming[^paradiagram]
* [ ] 采用`openapi` 规范的接口文档[^swagger]
    * [ ] 已有接口的详细描述
* [ ] api documentation (both http and websockets[^api])
* [ ] Data type validation

功能
- [x] 用热力图可视化豆瓣用户在小组话题里的活跃度
- [ ] 用热力图可视化git用户的commits的活跃度[^gitCommits]
    - [x] 收集数据[^gitlog]
    - [x] 数据处理成json

# JS debug
因为这个项目也包含了前端的代码，用到了原生js, d3, jquery等, 所以有关于调试JS的技术问题
1. type of variable
    1. `typeof var`
    2. `Object.prototype.toString`

        ```sh
        Object.prototype.toString.call(data)
        '[object Array]'
        ```

> In the Python world you don’t get the same level of control regarding developing new interaction techniques or heavily customizing visualizations that you do in the JavaScript world.

## JS ecosystem
1. JS standard[^jsv]
2. JS engine applied in browsers, `node.js`[^jse] (`V8 engine`, `SpiderMonkey`, ...)

# `Jinja` - template engine
how filters work?

```sh
In [14]: type(jinja2.Environment().filters)
In [15]: jinja2.Environment().filters
```


# D3 related
需求: 一个scalable的图

要使用`d3.js`, 对JS要有一定程度的了解，基础概念要清楚

https://eloquentjavascript.net/2nd_edition/

在练习d3的使用时，observable 是合适的工具，因为所有的改动都能在一个页面上呈现，不像你在本地需要在文本编辑器和网页间来回切换，这种体验就很逊色。

## D3 method chaining
你可能已经上d3一段时间, 会经常看到这种 `var yScale = d3.scale.ordinal().rangeBands([innerHeight, 0], barPadding);`
连续的方法调用，你可能隐约有个不感觉，这是函数编程的一个特点。每一步都返回一个值（对象），这个值又自带一些方法，确保你可以继续调用。

```js
var doh = "Doh";
console.log(typeof doh.toUpperCase);
// → function
console.log(doh.toUpperCase());
// → DOH
```

> Interestingly, even though the call to `toUpperCase` does not pass any arguments, the function somehow has access to the string "Doh", the value whose property we called. (Methods are simply properties that hold function values.)

https://www.d3indepth.com/selections/

> Most selection functions return the selection, meaning that selection functions such as `.style`, `.attr` and `.on` can be chained

找了个完整的项目　`git@github.com:NolanGit/PersonalHomepage.git`　

在template 这应用　Element-ui, 一直出不来样式, 在官网上找到了代码在线上测试，对比发现少写`<script>new Vue().$mount('#app2')</script>`

http://t.cn/A6MG4Vnw?m=4685317195960229&u=1748548681
## D3 reference
how to customize axes in d3? how to select axis? I only see axis generator!

依靠琐碎的知识和连蒙带猜是不行的，得看一个覆盖完整知识点的教程。
* http://www.carlosrendon.me/unfinished_d3_book/
* https://github.com/curran/dataviz-course-2018
* https://vizhub.com/
* https://www.d3-graph-gallery.com/graph/custom_axis.html
* http://using-d3js.com/

# `Vue`
安装vue 命令行工具:`npm --registry=https://registry.npm.taobao.org install @vue/cli@5.0.8`
## 原理[^mechanism]
1. component registration

2. template syntax
    1. v-for[^vfor]

## dependencies import
https://vueschool.io/articles/vuejs-tutorials/how-to-migrate-from-vue-cli-to-vite/
## 添加除默认页面之外页面的路由
不应该手动创建`router`文件夹[^router],需要依照`vue`的方式，因为`vue`会自动创建使用的例子。
![](./fig/router.png)

## `vuex.js`
## 运行环境
1. 选择docker容器
    ```sh
    docker pull ubuntu:jammy
    docker build -t mixed-backend:240404 -f app/server.Dockerfile .
    ```
2. local

    both `npm`, `node` need to updated to a proper version
    1. https://github.com/webpack/webpack/issues/15900
    2. https://stackoverflow.com/questions/63242821/not-running-at-local-when-npm-run-serve-command-in-vue-project

    ![](./fig/local_serve.png)
## debugging[^vscodedebug]
* [ ] container
    1. https://stackoverflow.com/questions/52377756/what-is-webroot-in-the-vscode-chrome-debugger-launch-launch-config
* [ ] local
    Debugging `vue` in `vscode`
* [ ] plugin
    1. https://devtools.vuejs.org/guide/installation.html

败了，都不会用，气！

因为 我设想的是在浏览器的souce tab 下设置断点是不成了，因为找不到vue项目文件，

所以之后的办法就都和vscode相关，在vscode里设置断点, 最后看还得是在浏览器里找到`vue`源文件(前端项目在本地或者容器中运行均可行，**要查看某个页面的源文件，需要先跳转到该页面**，再在source tab 下执行`command + P`)设置断点[^debugbrowsers]。
![](./fig/open_vue_in_browser.png)

## 目录结构
与其自己写一些`vue`, `js`, `html`文件, 然后使用`Jinja2Templates`host 相应页面，但为何不按照`vue`这个框架本身应用它，直接创建一个完整的前端工程。
1. `package.json`(类比于`Python`中的`requirements.txt`)
2. `src/router/index.js` 里设置页面的路由
3. `src/views` 存放用户看到的页面
```sh
➜ grep -rnw frontend/ -e "mapActions("
/src/views/Note.vue:32:    ...mapActions(['viewNote', 'deleteNote']),
/src/views/EditNote.vue:44:    ...mapActions(['updateNote', 'viewNote']),
/src/views/Dashboard.vue:71:    ...mapActions(['createNote']),
/src/views/Login.vue:30:    ...mapActions(['logIn']),
/src/views/Register.vue:35:    ...mapActions(['register']),
/src/views/Profile.vue:24:    ...mapActions(['deleteUser']),
```
# Reference
* [ ] [What is `DOM`?](https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model/Introduction)
* [ ] [d3 tutorial](https://alignedleft.com/tutorials/d3/fundamentals)
* [ ] [SVG cheatsheet](http://www.cheat-sheets.org/own/svg/index.xhtml)
* [ ] [SVG code online editor with preview](https://editsvgcode.com/)
* [ ] [HTML element](https://en.wikipedia.org/wiki/HTML_element)
* [ ] [JavaScript tutorial in an interactive way ](https://beta.observablehq.com/playground)
* [ ] [javascript](https://eloquentjavascript.net/)
* [x] https://learnlayout.com/
* [x] [html & CSS](https://learn.shayhowe.com/html-css)
* [ ] [Look up html tag css attribute and etc on MDN](https://developer.mozilla.org/zh-CN/docs/Web/CSS/vertical-align)
* [x] [interactive cheatsheet](https://htmlcheatsheet.com/) enable you to apply things out of box as a novice.
* [x] [Formal css template](https://purecss.io/layouts/)
* [x] [css component](https://freefrontend.com/css-headers-footers/#footer)
* [x] [css tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [x] [grapesjs](https://github.com/artf/grapesjs) forced to figure out how it works a year ago, the way it works was too strange for me at that time.
* [ ] https://github.com/visionmedia/move.js
* [x] gitlab flavored markdown https://docs.gitlab.com/ee/user/markdown.html
* [x] https://towardsdatascience.com/build-interactive-charts-using-flask-and-d3-js-70f715a76f93

# SQLAlchemy PostgreSQL JSON query[^json]
```sh
douban=# \d topic;
                                         Table "public.topic"
   Column    |            Type             | Collation | Nullable |              Default
-------------+-----------------------------+-----------+----------+-----------------------------------
 id          | integer                     |           | not null | nextval('topic_id_seq'::regclass)
 title       | character varying           |           |          |
 created_at  | timestamp without time zone |           | not null |
 author_url  | character varying           |           | not null |
 author_name | character varying           |           |          |
 comments    | json                        |           |          |
Indexes:
    "topic_pkey" PRIMARY KEY, btree (id)
    "topic_author_url_created_at_key" UNIQUE CONSTRAINT, btree (author_url, created_at)

```

```sql
SELECT "user".id AS user_id, "user".payment_info AS user_payment_info
FROM "user", jsonb_array_elements("user".payment_info -> %(payment_info_1)s) AS subs
WHERE (subs ->> %(subs_1)s) = %(param_1)s
```

```sh
douban=# select comments->2 from topic where author_name='吾爱';
```
## SQLAlchemy usage
```py
from sqlalchemy.types import Unicode
from sqlalchemy.sql.expression import func
(Pdb) Topic.query.filter(Topic.comments['reply_id'].astext.cast(Unicode) == "https://www.douban.com/people/141377840/")

(Pdb) db.session.query(Topic.id, func.json_array_length(Topic.comments))
(Pdb) db.session.query(Topic.id, Topic.author_name, func.json_array_length(Topic.comments))[1]
(1036, 1835)
(Pdb) db.session.query(Topic.id, Topic.author_name, func.json_array_length(Topic.comments))[1]
(1036, '老李家的崽', 1835)
(Pdb) Topic.query.filter(Topic.comments.contains([{"reply_name": "LTPE"}])).first()
*** sqlalchemy.exc.InternalError: (psycopg2.errors.InFailedSqlTransaction) current transaction is aborted, commands ignored until end of transaction block

[SQL: SELECT topic.id AS topic_id, topic.title AS topic_title, topic.created_at AS topic_created_at, topic.author_url AS topic_author_url, topic.author_name AS topic_author_name, topic.comments AS topic_comments
FROM topic
WHERE (topic.comments LIKE '%%' || %(comments_1)s || '%%')
 LIMIT %(param_1)s]
[parameters: {'comments_1': '[{"reply_id": "https://www.douban.com/people/141377840/"}]', 'param_1': 1}]
(Background on this error at: https://sqlalche.me/e/14/2j85)
# https://stackoverflow.com/a/2979389/7583919

(Pdb) Topic.query.filter(Topic.author_name=='LTPE', Topic.author_url =='https://www.douban.com/people/198717133/').first().title
...
(Pdb) Topic.query.filter(Topic.comments["reply_name"].astext.cast(Unicode) == "LTPE").count()
0
(Pdb) sql = "select * from topic where author_name='LTPE' and comments->>'reply_name'= 'LTPE'"
```
https://stackoverflow.com/questions/27288903/how-can-i-query-data-filtered-by-a-json-column-in-sqlalchemy
Why it is not working in my case?

`sqlaclchemy` engine 是在哪里构建的，没找到相应的代码？😯

原来在设置的环境变量`export APP_SETTINGS="config.DevelopmentConfig"`
```
(Pdb) db
<SQLAlchemy engine=postgresql://flashlight:***@localhost:5432/douban>
(Pdb) db.engine
Engine(postgresql://flashlight:***@localhost:5432/douban)
(Pdb) db.table(Topic)
<sqlalchemy.sql.selectable.TableClause at 0x11dbe1208; <class 'models.Topic'>>
(Pdb) db.table(Topic).is_selectable
True
(Pdb) db.engine.driver
'psycopg2'
(Pdb) db.engine.execute("select t.author_name, (t.comments::json->1)::json->'reply_name' from topic t limit 2;").all()
[('大白菜', '大白菜'), ('什么什么小姐(想一想)', '什么什么小姐')]
(Pdb) db.session.query(Topic).select_from(Topic, func.jsonb_array_elements(Topic.comments['reply_name']).alias()).filter(comments['reply_name'].astext == 'LTPE')
# https://stackoverflow.com/questions/34322471/sqlalchemy-engine-connection-and-session-difference
```

## Do query on JSON[^query]
```sh
douban=# select * from topic where author_name='吾爱' and comments->>'reply_name'='吾爱';
 id | title | created_at | author_url | author_name | comments
----+-------+------------+------------+-------------+----------
(0 rows)

douban=# select * from topic where author_name='吾爱' and comments->>'reply_name'='\u56bc\u51b0\u5757\u5927\u738b';
 id | title | created_at | author_url | author_name | comments
----+-------+------------+------------+-------------+----------
(0 rows)

douban=# select comments->3->'reply_id' from topic where author_name='吾爱';
                  ?column?
--------------------------------------------
 "https://www.douban.com/people/141377840/"
(1 row)


douban=# select * from topic where cast(comments->>'reply_name' as text) ='LTPE';
 id | title | created_at | author_url | author_name | comments
----+-------+------------+------------+-------------+----------
(0 rows)

douban=# select t.author_name, j from topic t, json_array_elements(t.comments::json) j limit 5;
douban=# select t.author_name, t.comments::json->-1 from topic t limit 2;
douban=# select t.author_name, (t.comments::json->1)::json->'reply_name' from topic t limit 2;
 author_name |             ?column?
-------------+----------------------------------
 yiyi        | "yiyi"
 老李家的崽  | "\u8001\u674e\u5bb6\u7684\u5d3d"
(2 rows)
douban=# SHOW SERVER_ENCODING;
 server_encoding
-----------------
 UTF8
(1 row)
douban=# show client_encoding;
 client_encoding
-----------------
 UTF8
(1 row)
douban=# select * from json_array_length('[1,2,3,{"f1":1,"f2":[5,6]},4]');
 json_array_length
-------------------
                 5
douban=# select * from json_each_text('{"a":"foo", "b":"bar"}');
 key | value
-----+-------
 a   | foo
 b   | bar
(2 rows)

douban=# select t.author_name, json_each_text(t.comments::json->2)  from topic t where author_name ='LTPE';
 author_name |                       json_each_text
-------------+-------------------------------------------------------------
 LTPE        | (reply_name,LTPE)
 LTPE        | (reply_id,https://www.douban.com/people/198717133/)
 LTPE        | (reply_time,"2019-08-25 21:01:17")
 LTPE        | (reply_content,她离婚了。)
 LTPE        | (fig_path,"")
 LTPE        | (quote,"{""quote_content"": """", ""quote_author"": """"}")
(6 rows)
douban=# select t.author_name, json_array_elements(t.comments::json) as comment_ls from topic t where author_name ='LTPE' limit 12;

douban=# select t.author_name, json_each_text(json_array_elements(t.comments))  from topic t where author_name ='LTPE';
```

# 容器化
Aimed at MACOS, `postgresql` image
```sh
± |master U:2 ?:4 ✗| → docker logs  -f dev_db
ls: cannot access '/docker-entrypoint-initdb.d/': Operation not permitted
```

1. 升级当前的docker, 可能是之前的一个问题，新一点的docker 已经解决这个问题了

    1. 从现有的docker桌面版找到更新的地方，发现下载速度极慢。
    2. 如果不符合官网上最新版本对操作系统的要求，找release note, 找一个稍微旧些的版本
    3. 下拉镜像(官方仓库)缓慢，可以考虑换源
2. `postgresql` init
    https://levelup.gitconnected.com/creating-and-filling-a-postgres-db-with-docker-compose-e1607f6f882f

`could not translate host name "dev_db" to address: Name or service not known`
* https://stackoverflow.com/questions/49737058/postgresql-sorting-json-array-by-numeric-key-value
* https://stackoverflow.com/questions/60000241/postgres-array-of-json-get-all-the-unique-values-of-a-specific-key


```sh
douban=# select id, count(id)  from topic cross join json_array_elements(comments) elem where (elem->>'reply_name') = 'LTPE' group by 1;
 id  | count
-----+-------
 218 |   461
(1 row)
douban=# select id, author_name from topic where author_name='LTPE';
 id  | author_name
-----+-------------
 218 | LTPE
(1 row)
douban=# select t.id, (select json_agg(distinct e.val ->> 'reply_name' order by e.val ->> 'reply_name') from json_array_elements(t.comments) as e(val)) as names from topic t limit 1;

douban=# select t2.id, t2.names as unique_names, json_array_length(names) as count from (select t.id, (select json_agg(distinct e.val ->> 'reply_name' order by e.val ->> 'reply_name') from json_array_elements(t.comments) as e(val)) as names from topic t) AS t2 order by t2.id limit 1;

douban=# select (select json_agg( e.val ->> 'reply_name' order by e.val ->> 'reply_name') from json_array_elements(t.comments) as e(val)) as names from topic t limit 1;
```

* https://www.postgresql.org/docs/14/sql.html

## Service architecture
- 从本地连接数据库容器服务（分清客户端和服务端）

    `psql -h localhost -p 4532 -d douban -U flashlight -W`
- 从本地连接本地数据库服务

    `psql -h localhost -p 5432 -d douban -U flashlight -W`
- 从flask应用容器连接数据库容器服务


In order to take a try of [`pyreverse`](https://stackoverflow.com/a/7554457/7583919), I add an empty file `__init__.py` under the root of project `flask_toy`,
## `Flask` vs `Fastapi`

https://towardsdatascience.com/understanding-flask-vs-fastapi-web-framework-fe12bb58ee75

我要能阐释到这个程度就算一个明显的进步了，按理说这个多数内容我都知道一些。

# Restart service after a long termination
## locally started
I was applying a not recommended practice, as I don't isolate an environment for this project, all dependencies is provided by my base environment.

一个好的程序，是在很久不用后，依然能顺利启动，能很快唤起当时的设计用意。不能像目前这样给出的线索，这些线索不够明朗(?)

```sh
psycopg2.OperationalError: could not connect to server: Connection refused
	Is the server running on host "localhost" (::1) and accepting
	TCP/IP connections on port 5432?
could not connect to server: Connection refused
	Is the server running on host "localhost" (127.0.0.1) and accepting
	TCP/IP connections on port 5432?
```

查看端口占用情况(check if port in use),
```sh
○ → lsof -i -P -n | grep LISTEN
```

Before launch flask application, make sure
1. `postgresql` service has started
    ```sh
    ± |master U:2 ?:5 ✗| → psql -d postgres -U wangmiao
    ```
2. `redis-server` has started

## Optimize the code structure
https://www.digitalocean.com/community/tutorials/how-to-structure-large-flask-applications

## Launch it as micro-service


[^1]: [更新后的css不起作用](https://stackoverflow.com/questions/22259847/application-not-picking-up-css-file-flask-python/63096673#63096673)
[^2]: 惊呆 flask `1.1.0` 于Jul 5, 2019发行, 我现在竟然在使用`1.0`，迅速升级到`2.0.1`
[^3]: 现在d3的例子中使用的数据都是直接读的csv文件，需要对数据做预处理才能拿到方便的数据结构，但受制于我贫瘠的js知识，处理起来很费劲，既然现在使用的是flask的后端，按理说我完全可以把数据处理成理想的json格式，传递给模板去渲染
[^4]: I have a serious problems, which I can only display one `vega-lite` example in a webpage, how to display multiple examples? https://github.com/altair-viz/altair/issues/1422
[^5]: https://dev.to/kiranrajvjd/awesome-css-generators-3709
[^database]: Instead of reading data from files, interact the backend with database. （lesson: Make sure you're looking up the right documentation which fits the library you're using)
[^async]: bad practice
    - load all the data only once.

    good practice
    - multiple times fetch, dynamically change web page

        https://stackoverflow.com/questions/17561243/jquery-selecting-dynamically-created-elements-and-pushing-to-firebase
    - cache to save the expensive loads
[^redis]: https://levelup.gitconnected.com/implement-api-caching-with-redis-flask-and-docker-step-by-step-9139636cef24
[^cssframework]: Use css framework

    Element UI 是一套采用 `Vue` 2.0 作为基础框架实现的组件库
    * https://cloud.tencent.com/developer/section/1489863
    * https://blog.csdn.net/weixin_43126568/article/details/107914613

    `npm` 库的流行趋势比较
    * https://www.npmtrends.com/echarts-vs-highcharts-vs-mxgraph-vs-plotly.js-vs-recharts-vs-vega-lite-vs-d3
    * https://blog.csdn.net/qq_34784833/article/details/109462425
    * https://www.zhihu.com/question/28687373/answer/225222545
[^json]:
    * `sqlalchemy` 对 `postgresql` 的支持现状: https://docs.sqlalchemy.org/en/14/dialects/postgresql.html
    * `postgresql` 的 JSON数据类型: https://www.postgresql.org/docs/13/functions-json.html
    * https://stackoverflow.com/questions/50330935/query-with-json-array-in-where-in-postgresql
    * https://stackoverflow.com/questions/27288903/how-can-i-query-data-filtered-by-a-json-column-in-sqlalchemy

[^query]:
    * https://stackoverflow.com/questions/54302827/using-jsonb-array-elements-in-sqlalchemy-query
    * https://www.enterprisedb.com/edb-docs/d/postgresql/reference/manual/11.9/functions-json.html

[^gitCommits]: 因为我觉得我去年十月一日我可能也在做这个项目
[^gitlog]: `git log --pretty=format:"%ci" > gitcommit.txt`
[^paradiagram]: * https://en.wikipedia.org/wiki/Event-driven_programming
    * https://en.wikipedia.org/wiki/DOM_events
    * https://tinyurl.com/58yx6wvp
[^api]: * https://github.com/tiangolo/fastapi/issues/1232
        * https://www.asyncapi.com/docs/tutorials/getting-started/servers
[^vue]: * https://youtube.com/watch?v=OrxmtDw4pVI
        * 友好的中文文档 https://cn.vuejs.org/guide/introduction.html
        * minimal sample https://testdriven.io/blog/combine-flask-vue/
        * middle difficulty https://6chaoran.github.io/data-story/visualization/data-engineering/fastapi-vue-app/
        * Harder https://testdriven.io/blog/developing-a-single-page-app-with-fastapi-and-vuejs/
[^render]: * [vue 和 jinja 变量在一起渲染](https://stackoverflow.com/questions/41082626/how-to-render-by-vue-instead-of-jinja)
    * https://vuejsexamples.com/tag/echarts/
[^jsv]: https://en.wikipedia.org/wiki/ECMAScript
[^jse]: https://en.wikipedia.org/wiki/JavaScript_engine
[^apig]: https://github.com/baranbartu/microservices-with-fastapi
[^router]: https://vueschool.io/articles/vuejs-tutorials/how-to-use-vue-router-a-complete-tutorial/

    自动创建minimal sample 的方式值得借鉴, 因为每个知识点配合一个轻巧的例子就能方便用户照猫画虎。

[^vfor]: * https://stackoverflow.com/a/62227846/7583919
    * https://vuejs.org/guide/essentials/list.html#v-for-with-an-object

[^swagger]: * https://www.linode.com/docs/guides/documenting-a-fastapi-app-with-openapi/
    * https://github.com/tiangolo/fastapi/issues/1140
[^vscodedebug]: * https://code.visualstudio.com/docs/containers/quickstart-node
* `Vue`debugging in  VS Code
* https://github.com/microsoft/vscode-recipes/tree/main/vuejs-cli
* deprecated https://github.com/Microsoft/vscode-chrome-debug#launch
* replacement https://github.com/microsoft/vscode-js-debug
* https://github.com/microsoft/vscode-js-debug/issues/854
* https://github.com/microsoft/vscode-js-debug/issues/1017
[^debugbrowsers]:  https://www.youtube.com/watch?v=7kWvjoubDp0
[^mechanism]: https://github.com/ustbhuangyi/vue-analysis
[^pd]: https://blog.logrocket.com/how-to-use-props-to-pass-data-to-child-components/
[^dynamicroute]: https://router.vuejs.org/guide/essentials/dynamic-matching.html
[^domain]: https://stackoverflow.com/questions/43741271/d3-change-and-update-axis-domain-scatterplot
[^mo]: https://bl.ocks.org/larsenmtl/e3b8b7c2ca4787f77d78f58d41c3da91
[^d3api]: d3 api is a bit tricky 
[^da1]: https://www.quora.com/Is-it-just-me-or-is-D3-js-too-hard
[^da2]: https://medium.com/dailyjs/the-trouble-with-d3-4a84f7de011f
[^skillset]: 需要熟悉`svg`的语法，实际上d3是在操作这些元素，添加元素，修改数据等,不能不熟悉这个概念就去写代码。因为不能将所有的操作归类到一个框架里
[^tj]: https://www.geeksforgeeks.org/how-do-you-run-javascript-script-through-the-terminal/
[^nrp]: https://www.nginx.com/blog/deploying-nginx-plus-as-an-api-gateway-part-1/
[^nrp2]: https://fastapi.tiangolo.com/advanced/behind-a-proxy/