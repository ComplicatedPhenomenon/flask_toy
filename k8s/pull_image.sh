docker pull gcr.io/knative-releases/knative.dev/serving/cmd/queue@sha256:f78383554ed81895ff230217f3e0ce9bf9ff2048d4303cc9fb36342ac3f470b3
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/queue@sha256:f78383554ed81895ff230217f3e0ce9bf9ff2048d4303cc9fb36342ac3f470b3
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/activator@sha256:24c6c8de9a6872ca796a13d1e8324a4dd250aacc5094975b60ce235122abb97f
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/autoscaler@sha256:f26a8b516112413cbba4244b36202354d1c98ed209301b255c55958213708a78
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/controller@sha256:ea48ea2f2433cc7e5c25940e79465ca7226750260faaa1724b95dd8cfac92034
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/domain-mapping@sha256:9197c51406208c8f3cc98c2b1f69ed2ba8b88e11cf765616700abecc5dd18350
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/domain-mapping-webhook@sha256:1b6e7f382c878f8ac168ce36a92f1af4dbdac0f61aae9e73fe899486786a4bbf
docker pull gcr.io/knative-releases/knative.dev/serving/cmd/webhook@sha256:e271d46b5168e25e9742f6f33a461cfcdc17b2460d4355fff7fe0c71fc1e4378


docker tag a3cdd4faac9d dolphinwm/webhook:20221220
docker tag e4ea927c455d dolphinwm/autoscaler:20221220
docker tag 9841a4249b9e dolphinwm/domain-mapping:20221220
docker tag 776f022c8a54 dolphinwm/activator:20221220
docker tag e5b384d3335b dolphinwm/domain-mapping-webhook:20221220
docker tag d9145b469584 dolphinwm/domain-mapping:20221220
docker tag 4de1796973ea dolphinwm/queue:20221220
docker tag 9841a4249b9e dolphinwm/controller:20221220

docker push dolphinwm/webhook:20221220
docker push dolphinwm/autoscaler:20221220
docker push dolphinwm/domain-mapping:20221220
docker push dolphinwm/activator:20221220
docker push dolphinwm/domain-mapping-webhook:20221220
docker push dolphinwm/domain-mapping:20221220
docker push dolphinwm/queue:20221220
docker push dolphinwm/controller:20221220