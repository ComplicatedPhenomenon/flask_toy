#!/bin/sh
docker build -t ezineo/nginx-gateway . && docker push ezineo/nginx-gateway
kn service create gateway -n knative-serving \
--image docker.io/ezineo/nginx-gateway \
--port 80 \
--scale-min 1 \
--force && \
kn domain create knative.10.1.0.2.sslip.io --ref ksvc:gateway:knative-serving -n knative-serving