## 安装基于kind的Knative Serving集群部署 (免代理一键快速部署)

**注意以下安装命令均需要在linux环境(包括wsl)下执行**

安装前置需求(具体在哪里下载怎么安装去谷歌搜)：

- docker
- kubectl
- kind
- kn

先给脚本添加执行权限: 

```bash
$ chmod +x ./create_knative_cluster.sh
$ chmod +x ./test_knative_helloworld.sh
```

然后执行一键安装脚本：

```bash
$ ./create_knative_cluster.sh
```

待安装完成后执行测试脚本：

```bash
$ ./test_knative_helloworld.sh
```

若最后一行为：

```bash
Hello Go Sample v1!
```

则表示Knative Serving已安装完成且正常运行。安装完成后，之后的操作可在windows环境(如Powershell和cmd)下进行。