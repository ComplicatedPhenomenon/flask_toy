#!/bin/bash
# kind create cluster --config clusterconfig.yaml -n knative && \
# kubectl apply -f serving-crds-1.8.0.yaml && \
# kubectl apply -f serving-core-1.8.0.yaml && \
# kubectl apply -f serving-hpa.yaml && \
# kubectl apply -f kourier.yaml && \
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress-class":"kourier.ingress.networking.knative.dev"}}' && \
kubectl --namespace kourier-system get service kourier && \
kubectl patch configmap/config-domain \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"127.0.0.1.sslip.io":""}}' && \
# kubectl patch configmap/config-domain \
#   --namespace knative-serving \
#   --type merge \
#   --patch '{"data":{"10.1.0.2.sslip.io":""}}' && \
kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"autocreate-cluster-domain-claims": "true"}}'
