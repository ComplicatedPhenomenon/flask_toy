https://stackoverflow.com/questions/44379805/kubernetes-has-a-ton-of-pods-in-error-state-that-cant-seem-to-be-cleared

```sh
➜ kubectl get pods -n knative-serving
➜ kubectl delete pods --field-selector="status.phase!=Succeeded,status.phase!=Running" -n knative-serving
➜ kubectl describe pods -n knative-serving controller-6b7c65ff48-v86zc
➜ kubectl get pods -n knative-serving
NAME                                      READY   STATUS         RESTARTS      AGE
activator-7b9bbdd575-k29p8                1/1     Running        1 (12m ago)   19m
autoscaler-85dd498969-27vp9               1/1     Running        0             19m
autoscaler-hpa-7f46fcdcc8-92qxc           1/1     Running        0             19m
controller-6b7c65ff48-v86zc               0/1     ErrImagePull   0             75s
domain-mapping-6c59cc9d-tnm6r             1/1     Running        0             19m
domainmapping-webhook-665bc48ddc-vnpxj    1/1     Running        0             19m
net-kourier-controller-7b8dbcbc59-5j8vj   1/1     Running        0             19m
webhook-55b4c97c7d-rnqpj                  1/1     Running        0             19m
➜ kubectl get pods -A
➜ kn service list -n knative-serving
NAME            URL                                                      LATEST                AGE     CONDITIONS   READY   REASON
helloworld-go   http://helloworld-go.knative-serving.10.1.0.2.sslip.io   helloworld-go-00002   4h13m   3 OK / 3     True
➜ kn service delete helloworld-go -n knative-serving
Service 'helloworld-go' successfully deleted in namespace 'knative-serving'.
➜ helm install my-opensearch -f opensearch/values.yaml opensearch-project-helm-charts/opensearch 
➜ helm uninstall my-opensearch
```