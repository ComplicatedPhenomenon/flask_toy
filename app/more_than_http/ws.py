import asyncio
from os import environ
from pprint import pprint
from random import sample
from typing import List, Dict

from fastapi import APIRouter, WebSocket
from redis import Redis

router = APIRouter()
stream_key = environ.get("STREAM", "jarless-1")
redis_cli = Redis("cache", 6379, retry_on_timeout=True, password='eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81')


@router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        # https://github.com/tiangolo/fastapi => fastapi/fastapi/websockets.py 
        # 可以看到fastapi 的依赖，这就仿佛苹果，巧妙的利用好现成的技术
        # https://www.starlette.io/websockets/
        # # 事件是不定时发生的, client 发送它就会有回应
        print('websocket.url', websocket.url)
        print('user-agent', websocket.headers['user-agent'])
        data = await websocket.receive_text()
        await websocket.send_text(f"Message text was: {data} " + sample(list('🐣🦕🤙🐸🙃🏴‍☠️🐤🐧🙈'), 1)[0])


@router.websocket("/ws1")
async def websocket_endpoint(websocket: WebSocket):
    """
    TODO:
    - Understand deeply the redis-py.xread method:
      - why we need to send a list of stream? I didn't want get the first [0] element
      - why it returns bytes, Is the .decode a good way?
      - is there a better way to convert it to python dict?
      - what is the a good number for the sleep/block?

    https://www.starlette.io/websockets/
    """
    last_id = 0
    sleep_ms = 5000

    await websocket.accept()
    while True:
        await asyncio.sleep(0.3)
        resp = redis_cli.xread({stream_key: last_id}, count=1, block=sleep_ms)
        print("Waitting...")
        if resp:
            key, messages = resp[0]  # :(
            last_id, data = messages[0]
            data_dict = {k.decode("utf-8"): data[k].decode("utf-8") for k in data}
            data_dict["id"] = last_id.decode("utf-8")
            data_dict["key"] = key.decode("utf-8")
            await websocket.send_json(data_dict)
