import datetime
import json
import re
from typing import List, Dict
from pprint import pprint

from fastapi import APIRouter
from fastapi_cache.decorator import cache
from pydantic import BaseModel
import requests

router = APIRouter()
ip = 'elasticsearch'
headers = {'Content-Type': 'application/json'}


class Job(BaseModel):
    job_id: str
    

@router.get("/topic/jobs_from_remoteok")
@cache(namespace="test", expire=120)
async def get_job_list()->List:
    """提供remoteok.com 获取的数据
    """
    query = {
            "size": 30,
            "query": {
                "match_all":{},
            },
            "sort": {"ts": "desc"}
        }
    index = "remoteok"
    url = f'http://{ip}:9200/{index}/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    data_ls = [ ]
    for i in resp.json()['hits']['hits']:
            data = i['_source']
            data['topic_id'] = i['_id']
            data['ts'] = str(datetime.datetime.fromtimestamp( int(data['ts']) ))
            data_ls.append(data)
    return data_ls
