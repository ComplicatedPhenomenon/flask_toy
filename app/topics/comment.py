from glob import glob
import json
from typing import List, Dict
from pprint import pprint

from fastapi import APIRouter
from fastapi.responses import ORJSONResponse
from fastapi_cache.decorator import cache
from pydantic import BaseModel
import requests

class Quote(BaseModel):
    quote_author: str
    quote_content: str


class Comment(BaseModel):
    reply_name: str
    quote: Quote
    reply_id: str
    reply_time: str
    fig_path: str
    reply_content: str

router = APIRouter()
ip = 'elasticsearch'
headers = {'Content-Type': 'application/json'}


def foo():
    query = {
        "size": 1000,
        "query": {
            "match_all":{}
        },
        "_source": {
            "excludes": ["comments"],
        }
    }
    index = "topic"
    url = f'http://{ip}:9200/{index}/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    data_ls = [ ]
    for i in resp.json()['hits']['hits']:
        try:
            data = i['_source']
            data['topic_id'] = i['_id']
            data_ls.append(data)
        except:
            pass
    return data_ls


@router.get("/topic/main")
@cache(namespace="test", expire=120)
async def get_topic_body():
    data_ls = foo()
    return data_ls


@router.get("/topic/main_2", response_class=ORJSONResponse)
@cache(namespace="test", expire=120)
async def get_topic_body():
    data_ls = foo()
    return ORJSONResponse(data_ls)



@router.get("/topic/comments/len", response_model=int)
async def get_comments_len(topic_id):
    '''
    获取话题下的评论数目

    **Error No field found for [comments] in mapping**
    1. https://discuss.elastic.co/t/no-field-found-for-in-mapping-error-with-correct-mapping/270499
    2. https://github.com/elastic/elasticsearch/issues/44692
    3. https://stackoverflow.com/questions/58643559/loop-through-items-with-painless-but-no-field-found

    **没一个解决version 8.4的问题**

    Finally, 数小时后
    1. 线索 https://discuss.elastic.co/t/how-to-get-array-size-for-each-document/226599/2
    2. 权威解释 https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-field-context.html#CO154-1

    **expensive queries**
    https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html

    Alternatives
    1. params['_source'].comments.getLength()
    2. params['_source'].comments.length

    关键词
    1. [`script_fields`](https://www.elastic.co/guide/en/elasticsearch/reference/8.4/search-fields.html#script-fields)

        > You can use the script_fields parameter to retrieve a script evaluation (based on different fields) for each hit.
    2. `query`
    3. `filter`
    4. `script`

    add item to array or remove item from array using scripts
    1. guts https://www.youtube.com/watch?v=aVEvrZY3RTo
    2. samples https://pastebin.com/GdgN4WXt

    用来查询所有JAVA类的方法
    * https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-api-reference-shared.html
    * https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-operators-array.html
    '''
    query = {
                "query": {
                    "match": {"_id": topic_id}
                },
                "script_fields": {
                    "number_of_comments": {
                        "script": {
                            "source": "params['_source'].comments.size()",
                        }
                    }
                },
                "_source": {
                    "includes": [ "fields" ]
                }
            }

    url = f'http://{ip}:9200/topic/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    tem = resp.json()['hits']['hits']
    num = 0
    if tem:
        num = tem[0]['fields']['number_of_comments'][0]
    return num


@router.get("/topic/comment_columns")
async def get_topic_comments_columns_via_sql_query(
    author_id:str = "https://www.douban.com/people/115838573/"):
    '''
    获取comments 的columns

    为甚么doc["comments"]不行，params['_source']行
    1. https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting-fields.html#modules-scripting-doc-vals
    2. https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-scripting-fields.html#modules-scripting-source

        > The `_source` is loaded as a map-of-maps, so properties within object fields can be accessed as, for example, _source.name.first.

        {'error': {'root_cause': [{'type': 'verification_exception', 'reason': 'Found 1 problem line 1:8: Cannot use field [comments.quote] type [object] only its subfields'}], 'type': 'verification_exception', 'reason': 'Found 1 problem line 1:8: Cannot use field [comments.quote] type [object] only its subfields'}, 'status': 400}

    https://www.elastic.co/guide/en/elasticsearch/reference/current/sql-limitations.html#_scalar_functions_on_nested_fields_are_not_allowed_in_where_and_order_by_clauses
    '''
    print(requests.post(f'http://{ip}:9200/_sql?format=txt', headers=headers, data=json.dumps({"query": "show tables"})).text)

    print(requests.post(f'http://{ip}:9200/_sql?format=txt',
        headers=headers, data=json.dumps({"query": "describe topic"})).text)
    # 注意sql中好像要用单引号
    query = {
        "query": f"SELECT comments.* FROM topic WHERE author_id='{author_id}'"
    }
    url = f'http://{ip}:9200/_sql'
    resp = requests.post(url, headers=headers, data=json.dumps(query))

    print('comment rows number: ', len(resp.json()['rows']))
    # query = {
    #     "query": f"SELECT comments.* FROM topic WHERE author_id='{author_id}' ORDER BY comments.reply_time DESC"
    # }
    # resp = requests.post(url, headers=headers, data=json.dumps(query))
    return resp.json()['columns']


@router.get("/topic/comments", response_model=List[Comment])
async def get_topic_comments_via_dsl(
    topic_id,
    page:int = 0,
    page_size:int = 20):
    '''
    获取某个话题下的评论，评论分页
    '''
    query = {
        "query": {
            "match": {"_id": topic_id}
        },
        "_source": {
            "includes": ["comments"],
        }
    }
    url = f'http://{ip}:9200/topic/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    resp = resp.json()['hits']['hits']
    comments = resp[0]['_source']['comments'] if resp else 0
    print('comments num: ', len(comments))
    return comments[page_size*page: page_size*(page+1)]


@router.get("/topic/comments_info")
async def get_topic_comments_via_script_query(
    topic_id:str):
    # https://www.elastic.co/guide/en/elasticsearch/painless/current/painless-api-reference-shared-java-util.html#painless-api-reference-shared-ArrayList
    zed = {
            "reply_name": "[已注销]",
            "quote": {
            "quote_author": "",
            "quote_content": ""
            },
            "reply_id": "https://www.douban.com/people/loveruandme/",
            "reply_time": "2017-10-12 21:33:09",
            "fig_path": "",
            "reply_content": "非常无聊厚"
           }

    zed2 = {
            "reply_name": "假的ʕ•ᴥ•ʔ",
            "quote": {
            "quote_author": "",
            "quote_content": ""
            },
            "reply_id": "https://www.douban.com/people/115838573/",
            "reply_time": "2017-10-12 11:10:12",
            "fig_path": "",
            "reply_content": "假的假的"
        }
    query = {
                "query": {
                    "match": {"_id": topic_id}
                },
                "script_fields": {
                    "isEmpty": {
                        "script": {
                            "source": "params['_source'].comments.isEmpty()"
                        }
                    },
                    "size": {
                        "script": {
                            "source": "params['_source'].comments.size()"
                        }
                    },
                    "subList": {
                        "script": {
                            "source": "params['_source'].comments.subList(0, 3)"
                        }
                    },
                    "contains": {
                        "script": {
                            "source": "params['_source'].comments.contains(params['zed'])",
                            "params": {
                                "zed": zed
                            }
                        },
                    },
                    "lastIndexOf": {
                        "script": {
                            "source": "params['_source'].comments.lastIndexOf(params['zed'])",
                            "params": {
                                "zed": zed
                            }
                        },
                    },
                    "temporaryAdd":{
                        "script": {
                            "source": """
                                        if (params._source.comments==null){
                                            params._source.comments.remove(2);
                                        }
                                        else{
                                            params._source.comments.add(0, params['zed']);
                                            params._source.comments[0];
                                        }
                                    """,
                            "params": {
                                "zed": zed2
                            }
                        }
                    },
                }
            }
    url = f'http://{ip}:9200/topic/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    return resp.json()


@router.post("/topic/comments")
async def update_topic_comments_via_script_query(
    comment: Comment,
    doc_id:str= 'AVwC64MBbNVwHH7_UYr5'):
    '''
    comment sample
    ```json
    {
        "reply_name": "假的ʕ•ᴥ•ʔ",
        "quote": {
        "quote_author": "",
        "quote_content": ""
        },
        "reply_id": "https://www.douban.com/people/115838573/",
        "reply_time": "2017-10-12 11:10:12",
        "fig_path": "",
        "reply_content": "假的假的"
    }
    ```

    https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update.html
    '''
    query = {
                "script": {
                    "source":"""
                             ctx._source.comments.add(0, params['zed'])
                             """,
                    "params": {
                        "zed": eval(comment.json())
                    }
                }
            }
    url = f'http://{ip}:9200/topic/_update/{doc_id}'
    resp = requests.post(url, headers=headers, data=json.dumps(query))
    return resp.json()


@router.get("/topic/authors")
async def get_topic_author_ids():
    query = {
        "size": 1000,
        "query": {
            "bool": {
                "must": []
            }
        },
        "_source": {
            "includes": [ "author_id"],
        }
    }
    url = f'http://{ip}:9200/topic/_search'
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    names = [ ]
    for i in resp.json()['hits']['hits']:
        try:
            name = i['_source']['author_id']
            names.append(name)
        except:
            print(name)
            pass
    return names