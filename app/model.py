from sqlalchemy.dialects.postgresql import ARRAY, JSON, JSONB
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

# https://stackoverflow.com/questions/42909816/can-i-avoid-circular-imports-in-flask-and-sqlalchemy
class Topic(db.Model):
    __tablename__ = 'topic'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String)
    created_at = db.Column(db.DateTime, nullable=False)
    author_url = db.Column(db.String(), nullable=False)
    author_name = db.Column(db.String())
    comments = db.Column(JSON)
    db.UniqueConstraint(author_url, created_at)
    # https://docs.sqlalchemy.org/en/14/orm/constructors.html


class Movie(db.Model):  # 表名将会是 movie
    __table_args__ = (
        # this can be db.PrimaryKeyConstraint if you want it to be a primary key
        db.UniqueConstraint('title'),
      )
    id = db.Column(db.Integer, primary_key=True)  # 主键
    title = db.Column(db.String(60))  # 电影标题
    year = db.Column(db.String(4))  # 电影年份
