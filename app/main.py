import json
import pandas as pd


if __name__ =='__main__':
    df = pd.read_csv('./data/csv/religionByCountryTop20.csv')
    df2 = df.pivot(index='country', columns='religion')['population']
    breakpoint()
    df2.columns = ['_'.join(k.split(' ')) for k in df2.columns]
    ids = df2.sum(axis=1).sort_values(ascending=False).index
    groups = json.dumps(list(ids))
    subgroups = json.dumps(list(df2.columns))
    # 制造方便调用d3.stack()的数据结构
    vals = df2.reset_index().values
    cols = df2.reset_index().columns
    data = [dict(zip(cols, val)) for val in vals]
    data = json.dumps(data)