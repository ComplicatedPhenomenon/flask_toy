import time
import logging
import re

# https://book.pythontips.com/en/latest/args_and_kwargs.html
def timeConsumed(a_func):
    def wrapTheFunction(*args, **kwargs):
        a = time.time()
        res = a_func(*args, **kwargs)
        b = time.time()
        print(f'{a_func.__name__} takes {round(b-a, 1)} seconds')
        return res
    return wrapTheFunction


def get_logger():
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
            fmt = '[%(asctime)s] | file: %(filename)-12s | line: %(lineno)d |\n%(message)s',\
            datefmt = "%Y-%m-%d %H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger


def get_sql_statements():
    with open('data/douban.sql','r') as f:
        sql_ls = f.readlines()
    sql_ls = re.compile('\/\*.*\/\\n').split(''.join(sql_ls))[1:]   
    return sql_ls
