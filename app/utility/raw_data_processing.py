from collections import defaultdict
import json

import pandas as pd


def _preprocess_df(df):
    # 统一DataFrame的列名
    cols = ['name', 'url', 'created_time', 'text', 'pic_links', 'quote']
    unified_cols = ['reply_name', 'reply_id', 'reply_time', 'reply_content', 'fig_path', 'quote']
    if list(df.columns) != unified_cols:
        df = df.rename(dict(zip(cols, unified_cols)), axis=1)
    df = df.drop_duplicates(subset=['reply_time']).reset_index(drop=True)
    df.reply_time = pd.to_datetime(df.reply_time)
    df = df.sort_values('reply_time')
    return df


class statistic_on_participants():
    def __init__(self, df, file_name):
        self.df = df
        self.file_name = file_name
        self.data = []
        # 体现出 self.df2 与 self.df 属性的不同
        self.df2 = self.df[self.df.reply_name != self.file_name]


    def get_special_participants(self):
        self.df.reply_time = pd.to_datetime(self.df.reply_time)
        ps = self.df.groupby('reply_id').reply_name.apply(set)
        if ps.map(len).value_counts().shape[0]!=1:
            id_name_dicts = ps.map(list).map(lambda item: item[0]).to_dict()
            self.df.reply_name = self.df.reply_id.map(id_name_dicts)
        id_name_dict = self.df.groupby([self.df.reply_id, self.df.reply_name]).groups.keys()
        id_name_dict = {i[0]:i[1] for i in id_name_dict}
        # 谁的发言最长
        data = []
        ps = self.df.groupby(self.df.reply_id)['reply_content']\
            .apply(list).map(' '.join).map(len).sort_values(ascending=False).iloc[:10]
        data.append(ps)

        # 谁关注的最频繁
        tem = self.df.groupby(by=[self.df.reply_id, self.df.reply_time.dt.date, \
                        self.df.reply_time.dt.hour]).size().sort_values(ascending=False).index.tolist()
        ps = pd.DataFrame(tem).groupby(0).size().sort_values(ascending=False).iloc[:10]
        data.append(ps)
        
        #谁的发言被引用最多
        zed = self.df[self.df['quote'].map(lambda i: i['quote_content']!='' if 'quote_content' in i else i['content']!='')]
        ps = zed['quote'].map(lambda i: i['quote_author'] if  'quote_author' in i else i['url']).value_counts().iloc[:10]
        data.append(ps)

        participants = set()
        tag_ps_ls = []
        tag = ["comments longest, times", "most often visit, date hour nums", "most being quoted, times"]
        for j, ps in enumerate(data):
            ids = ps.index.tolist()
            # 历史bug, 在cleaner_people_profile中_get_people_profile url 为 self.name
            names = [id_name_dict[i] if i in  id_name_dict else i for i in ids ]
            participants = participants.union(names)
            ps.index = names
            tag_ps_ls.append((tag[j], ps))
        return participants, tag_ps_ls
    

    def filter_data(self):
        people, data = self.get_special_participants()
        ps = self.df.groupby(by=[self.df.reply_name, self.df.reply_time.dt.date,\
                                 self.df.reply_time.dt.hour]).size()

        ps.index.names = ['name', 'date', 'hour']
        ps = ps.sort_index(level=[0, 1, 2])

        x = ps.index.get_level_values('name').isin(people)
        df = ps[x].reset_index().rename({0: 'value'}, axis=1)
        df.loc[:, 'timestamp'] = pd.to_datetime(df.date.astype(str) + df.hour.astype(str).map(lambda i: i.zfill(2)), format='%Y-%m-%d%H')
        df = df[['name', 'timestamp', 'value']]
        return df


    def _statistics_on_num_vs_time(self, person):
        '''
        统计
        0. 所有日期压缩到一天的活跃度
        1. 每个日期， 每小时的活跃度
            0. 直方图
            1. 折线图
        '''
        file_name = 'static/data/csv' + f'/{person}'
        lz_df = self.df[self.df.reply_name == person][['reply_time']]

        distribution_by_hour_ps = lz_df.groupby(lz_df.reply_time.dt.hour).size()
        # 补齐所有时间段的情况
        hours = list(range(24))
        distribution_by_hour_ps = distribution_by_hour_ps.reindex(hours, fill_value=0)
        
        ext = 'hour'
        data1 = (distribution_by_hour_ps, file_name, ext)

        distribution_by_date_ps = lz_df.groupby(by=[lz_df.reply_time.dt.date]).size()
        dates = pd.date_range(lz_df.reply_time.min().date(), lz_df.reply_time.max().date(), freq='1D')
        distribution_by_date_ps = distribution_by_date_ps.reindex(dates, fill_value=0)

        ext = 'date'
        data2 = (distribution_by_date_ps, file_name, ext)

        start, end = lz_df.reply_time.dt.date.agg([min,max]).values
        dates = pd.date_range(start=str(start), end=str(end), freq='1D')
        dist_by_date = lz_df.groupby(by=[lz_df.reply_time.dt.date])
        distribution_by_date_hour_ps = []
        for date in dates:
            try:
                lz_df = dist_by_date.get_group(date)
                distribution_by_hour_ps = lz_df.groupby(lz_df.reply_time.dt.hour).size()
                hours = list(range(24))
                distribution_by_hour_ps = distribution_by_hour_ps.reindex(hours, fill_value=0)
                distribution_by_date_hour_ps.append((str(date.date()), distribution_by_hour_ps))
            except KeyError:
                distribution_by_date_hour_ps.append((str(date.date()), pd.Series([0 for _ in range(24)])))
        # date_hour_plot(distribution_by_date_hour_ps, f'{file_name}_date_hour_dist')
        # 判断是否连续性熬夜
        # z.groupby(x.reply_time.dt.hour).get_group(2).groupby(tem.reply_time.dt.date).size().shape[0]
        return data1, data2, distribution_by_date_hour_ps



class statistics_on_a_person():
    def __init__(self, df, path, file_name):
        self.df = df
        self.path = path
        self.file_name = file_name

 
    def do_statistic_2(self):
        '''
        生成作者的词云
        '''
        self.df = self.df.reset_index(drop=True)
        zed = self.df['reply_content']
        word_counts = zed.map(len).sum()
        empty_quote = {'quote_content': '', 'quote_author': ''}
        speak_her_mind = self.df[(self.df.quote==empty_quote)][['reply_time', 'reply_content']]
        num_0 = speak_her_mind.shape[0]
        total_num = self.df.shape[0]
        percentage = round(num_0/total_num, 2)

        if 'quote_author' in self.df.columns:
            people_id = self.df[(self.df.quote!=empty_quote)]['quote']\
                        .map(lambda item: item['quote_author']).value_counts()
            top_10_id = people_id.iloc[:10].index
            people_been_here = self.df['reply_id'].value_counts()
            name_id = self.df.groupby(['reply_name', 'reply_id']).groups.keys()
            id_name_map = {i[1]:i[0] for i in name_id}
            top_10_name = [id_name_map[i] for i in top_10_id]

        # based on every participants, yes including the author, generate the corresponding word cloud
        content = '\n'.join(zed.values.tolist())
        file_name = f'{self.path}{self.file_name}/{self.file_name}'



    def do_statistic_3(self):
        '''
        这个方法是对文本的分析，针对每个楼主，因为没有一个通用的方法，需要人工定制做不一样的分析
        '''
        x = self.df[(self.df.reply_name==self.file_name) & (self.df['reply_content'].str.contains(r'阿姨|她|闺蜜|他'))]\
            [['reply_content']].iloc[12,:].reply_content
    
        words=[r'老婆', r'吻']
        for word in words:
            zed = self.df[(self.df.reply_name==self.file_name) & \
                        (self.df['reply_content'].str.contains(word))]\
                            [['reply_content', 'reply_time']]
            x = zed.values.tolist()
            x = [f'{str(i[1])[5:]}:\t {i[0]}' for i in x]
            # print('\n\n'.join(x))

        word = ['英语', '单词', '@冰果音乐']
        for i in words:
            zed = self.df[(self.df.reply_content.str.contains(r'英语'))]['reply_content'].shape[0]
            # print(zed)


class cleaner_topic_reply():
    def __init__(self, file_name):
        self.path = 'data/crawler/douban_group_topic/'
        self.file_name = file_name


    def _get_data_for_topic_reply(self, file_name):
        file = self.path + f'{file_name}/{file_name}.json'
        with open(file, 'r') as f:
            data = json.load(f)
        return data


    def _get_topic_reply(self):
        data = self._get_data_for_topic_reply(self.file_name)
        df = pd.DataFrame(data['comments'])
        df = _preprocess_df(df)
        title = data['title'] + '\n    --' + str(data['published_time'])
        return title, df


    def get_main_comments_df(self, df):
        '''
        分离出帖子的主体
        '''
        if self.file_name == 'Fly':
            cp = ['Fly', 'Ylf']
        else:
            cp = [self.file_name]
        condition_1 = (df.reply_name.isin(cp)) & ((df.reply_content.str.len() > 20) | \
                (df.reply_content.str.len()==0)) & \
                (df.quote=={'quote_content': '', 'quote_author': ''})
        condition_2 = (~df.reply_name.isin(cp)) & (df.reply_content.str.len()>100)
        df = df[condition_1 | condition_2]
        df = df[['reply_name', 'reply_time', 'reply_content', 'fig_path', 'quote']]
        return df


    def _customized_filter_2(self, df):
        '''
        分离出帖子的用心回复
        '''
        path = f'{self.path}/{self.file_name}/'
        si = statistic_on_participants(df[df.reply_name!=self.file_name], path, self.file_name)
        individuals, _ = si.get_special_participants()
        if self.file_name == 'Fly':
            individuals = [i for i in individuals if i not in ['Ylf']]
            ps = df[df.reply_name.isin(individuals)].groupby('reply_name')['reply_content'].apply(list).map('\n'.join)
        else:
            ps = df[df.reply_name != self.file_name].groupby('reply_name')['reply_content'].apply(list).map('\n'.join)
        contents = ''
        for i, row in ps.iteritems():
            contents += '\n-----------------------\n'
            contents += f'\nauthor: {i}\n\n'
            row = row.replace('\n', '\n\n')
            if '*' in row:
                row = row.replace('*', '`*`')
            contents += f'{row}\n\n'
        return contents


    def get_statistics(self, vis=True):
        _, df =  self._get_topic_reply()
        if vis:
            si = statistic_on_participants(df, self.path, self.file_name)
            si.filter_data()
            si.visualize_statistics()
        return df

    def get_title_and_comments(self):
        title, comments =  self._get_topic_reply()
        vice_comments = self._customized_filter_2(comments)
        main_comments = self.get_main_comments_df(comments)
        return title, main_comments


class cleaner_people_profile():
    def __init__(self, file_name):
        self.path = 'data/crawler/douban_people_profile/'
        self.file_name = file_name


    def _get_people_profile(self):
        with open(self.path + f'{self.file_name}/{self.file_name}.json', 'r') as f:
            data = json.load(f)

        res_item = defaultdict(list)
        for item in data:
            # item 是发布的一条心情
            # item 类型为字典, 有四个key
            res_item['name'].append(self.file_name)
            res_item['url'].append('')
            res_item['created_time'].append(item.get('created_time'))
            res_item['text'].append(item.get('text', ''))
            res_item['pic_links'].append(item.get('pic_links', []))
            res_item['quote'].append({'content':'', 'url':''})
            res_item['video_links'].append(item.get('video_links', []))

            df = pd.DataFrame(item['comments'])
            for comment in df.iterrows():
                # 每个心情下的一级评论
                zed = comment[1][['name','url', 'created_time', 'text']].to_dict()
                res_item['name'].append(zed['name'])
                res_item['url'].append(zed['url'])
                res_item['created_time'].append(zed['created_time'])
                res_item['text'].append(zed['text'])
                res_item['pic_links'].append([])
                res_item['video_links'].append([])
                res_item['quote'].append({'content': item.get('text', ''), 'url': self.file_name})

                follow_comment = comment[1]['replies']
                # 每条一级评论下的子评论
                if follow_comment:
                    df_2 = pd.DataFrame(follow_comment)
                    df_2 = df_2.sort_values(['created_time'])
                    for sub_comment in df_2.iterrows():
                        zed_2 = sub_comment[1][['name', 'url', 'created_time',
                                            'reply_content', 'refer_to']]
                        res_item['name'].append(zed_2['name'])
                        res_item['url'].append(zed_2['url'])
                        res_item['created_time'].append(zed_2['created_time'])
                        res_item['text'].append(zed_2['reply_content'])

                        refer_to = {'content': zed['text'], 'url': zed['url']}
                        refer_to_2 = zed_2['refer_to']
                        # 依据@的对象，找出在时间上距离此条子评论最近的那条子评论
                        if refer_to_2:
                            name_url = [(k, v) for k, v in refer_to_2.items()]
                            refer_to['url'] = name_url[0][1]
                            content = df_2[(df_2.url == name_url[0][1]) & \
                                        (df_2.created_time < zed_2['created_time'])]\
                                        .iloc[-1, :]['reply_content']
                            refer_to['content'] = content

                        res_item['quote'].append(refer_to)
                        res_item['pic_links'].append([])
                        res_item['video_links'].append([])
        df_res_item = pd.DataFrame(res_item)
        return df_res_item


    def get_people_profile(self):
        df = self._get_people_profile()
        df = _preprocess_df(df)
        return df


    def get_statistics(self):
        df = self.get_people_profile()
        si = statistic_on_participants(df, self.path, self.file_name)
        si.visualize_statistics()

