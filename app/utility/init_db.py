from glob import glob
import json
from pprint import pprint
from time import sleep

import click
import requests


def init_mapping():
    '''
    an array of objects
    https://www.elastic.co/guide/en/elasticsearch/reference/current/array.html
    https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html#_limits_on_nested_mappings_and_objects

    {'error': {'root_cause': [{'type': 'mapper_parsing_exception', 'reason': 'The number of nested documents has exceeded the allowed limit of [10000]. This limit can be set by changing the [index.mapping.nested_objects.limit] index level setting.'}], 'type': 'mapper_parsing_exception', 'reason': 'The number of nested documents has exceeded the allowed limit of [10000]. This limit can be set by changing the [index.mapping.nested_objects.limit] index level setting.'}, 'status': 400}
    '''
    time_format = "strict_date_optional_time||basic_date||epoch_millis||yyyy-mm-dd HH:MM:SS||yyyy/mm/dd"
    query = {
            "mappings": {
                "properties": {
                    "comments": { 
                        "type": "nested",
                        "properties": {
                            "reply_name": {"type":"keyword"},
                            "quote": {"type": "nested"},
                            "reply_id": {"type":"keyword"},
                            "reply_time": {"type":"date", "format": time_format},
                            "fig_path": {"type":"keyword"},
                            "reply_content": {"type":"keyword"}
                        }
                    },
                    "published_time": {"type":"date", "format": time_format},
                    "author_name": {"type":"keyword"},
                    "author_id": {"type":"keyword"},
                    "title": {"type":"keyword"}
                }
            },
            "settings": {"index.mapping.nested_objects.limit": 100000}
        }
    url = f'http://{ip}:9200/topic'
    resp = requests.put(url, headers=headers, data=json.dumps(query))
    assert resp.status_code == 200
    pprint(requests.get(f'http://{ip}:9200/topic/_mapping').json())


def init_data():
    '''
    将数据快捷存入数据库, 在本地（而非容器当中）执行
    linux: hostname -I | awk '{print$1}'
    mac: ipconfig getifaddr $(route get 8.8.8.8 | awk '/interface: / {print $2; }')
    '''
    fs_ls = glob('./data/crawler/douban_group_topic/*/*.json')
    data = []
    for file in fs_ls:
        print(file)
        tem = json.load(open(file, 'r'))
        data.append({'create':{'_index': 'topic'}})
        data.append(tem)
        resp = requests.post(f'http://{ip}:9200/topic/_doc', headers=headers, data=json.dumps(tem))
        try:
            assert resp.status_code == 201
        except:
            breakpoint()
    payload = '\n'.join([json.dumps(line) for line in data]) + '\n'
    resp = requests.post(f'http://{ip}:9200/_bulk', headers=headers, data=payload)
    pprint(resp.json())


def emptify_opensearch():
    ip = '192.168.0.101'
    uri = f'http://{ip}:9200/_cat/indices?v'
    resp = requests.get(uri).text.strip().split('\n')
    indices1 = [i.split()[2] for i in resp]
    indices = [index for index in indices1 if index not in ['index', '.geoip_databases']]
    print(indices)
    for index in indices:
        resp = requests.delete(f'http://{ip}:9200/{index}')
        print(f'🏔 Delete index {index}')
        assert resp.json()['acknowledged']

    # for index in ['topic']:
    #     resp = requests.put(f"http://{ip}:9200/{index}")
    #     assert resp.status_code == 200


def test_init():
    query = {
            "mappings": {
                "properties": {
                "productId": {
                    "type": "integer"
                },
                "productName": {
                    "type": "text",
                    "fields": {
                        "raw": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                    }
                },
                "warehouses": {
                    "properties": {
                    "location": {
                        "type": "text"
                    },
                    "quantity": {
                        "type": "integer"
                    }
                    }
                }
                }
            }
    }

    resp = requests.put(f'http://{ip}:9200/products', headers=headers, data=json.dumps(query))
    print(resp.status_code)

    zed = []
    zed += [{"index" : {"_id":1}}]
    zed += [{"productId":1,"productName":"Bags","warehouses":[{"location":"Location A","quantity":20},{"location":"Location B","quantity":30},{"location":"Location C","quantity":50}]}]
    zed += [{"index" : {"_id":2}}]
    zed += [{"productId":2,"productName":"Shirts","warehouses":[{"location":"Location A","quantity":100},{"location":"Location B","quantity":150},{"location":"Location C","quantity":150}]}]
    zed += [{"index" : {"_id":3}}]
    zed += [{"productId":3,"productName":"Shoes","warehouses":[{"location":"Location A","quantity":100},{"location":"Location B","quantity":10},{"location":"Location C","quantity":50}]}]
    payload = '\n'.join([json.dumps(line) for line in zed]) + '\n'
    resp = requests.post(f'http://{ip}:9200/products/_bulk', headers=headers, data=payload)
    print(resp.status_code)


    query = {
             "query": {
                "bool": {
                "must": {
                    "match_all": {}
                },
                "filter": {
                    "bool": {
                    "must": {
                        "script": {
                        "script": {
                            "lang": "painless",
                            "source": """
                            def x = doc['warehouses.quantity'];
                            def flag = false;
                            for(int i = 0; i < x.length; i++) {
                                if(x[i] <= params.limit) {
                                flag = true;
                                }
                            }
                            return flag;
                            """,
                            "params": {
                            "limit": 50
                            }
                        }
                        }
                    }
                    }
                }
                }
            }
            }
    sleep(1)
    resp = requests.post(f'http://{ip}:9200/products/_search', headers=headers, data=json.dumps(query))
    print(resp.json())


if __name__ == '__main__':
    ip = '192.168.0.101'
    headers = {'Content-Type': 'application/json'}
    emptify_opensearch()
    init_mapping()
    init_data()
    # test_init()