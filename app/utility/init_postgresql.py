from functools import partial
from glob import glob
import json
from utility.utils import timeConsumed

# https://flask-sqlalchemy.palletsprojects.com/en/2.x/queries/
@timeConsumed
def create_db(db, Movie, Topic):
    db.engine.dialect._json_deserializer = partial(json.dumps, ensure_ascii=False)
    json.dumps = partial(json.dumps, ensure_ascii=False)
    # it works, 今天圆满了，没有直接上SO问，自己解决了这个问题
    # https://stackoverflow.com/a/58448439/7583919
    try:
        db.session.query(Movie).delete()
        db.session.query(Topic).delete()
        db.session.commit()
    except Exception as e:
        print(str(e))
        breakpoint()
    # https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.SQLAlchemy.create_all
    db.drop_all()
    db.create_all()
    movies = [
        {'title': 'My Neighbor Totoro', 'year': '1988'},
        {'title': 'Dead Poets Society', 'year': '1989'},
        {'title': 'A Perfect World', 'year': '1993'},
        {'title': 'Leon', 'year': '1994'},
        {'title': 'Mahjong', 'year': '1996'},
        {'title': 'Swallowtail Butterfly', 'year': '1996'},
        {'title': 'King of Comedy', 'year': '1999'},
        {'title': 'Devils on the Doorstep', 'year': '1999'},
        {'title': 'WALL-E', 'year': '2008'},
        {'title': 'The Pork of Music', 'year': '2012'}
    ]
    for m in movies:
        movie = Movie(title=m['title'], year=m['year'])
        db.session.add(movie)
        db.session.commit()

    # https://stackoverflow.com/questions/58003926/flask-sqlalchemy-add-multiple-row
    fs_ls = glob('./data/crawler/douban_group_topic/*/*.json')
    for file in fs_ls:
        print(file)
        data = json.load(open(file, 'r'))
        try:
            topic = Topic(title=data['title'], created_at=data['published_time'],
              author_name=data['author_name'], author_url=data['author_id'], comments=data['comments'])
        except:
            pass

        db.session.add(topic)
        db.session.commit()
    print('🐗 Database init is done!')
