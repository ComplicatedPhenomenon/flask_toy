FROM ubuntu:jammy

COPY source_conf/pip.conf /usr/local/src/flask_toy/
COPY source_conf/sources.list.163 /usr/local/src/flask_toy/
COPY requirements.txt /usr/local/src/flask_toy/

WORKDIR /usr/local/src/flask_toy/
RUN mv /etc/apt/sources.list /etc/apt/sources.list.bak && mv sources.list.163 /etc/apt/sources.list \
    && mkdir ~/.pip && mv pip.conf ~/.pip/pip.conf \
    && apt-get update \
    && apt-get install -y build-essential python3.11 python3-pip python3-dev python3-dotenv \
    && ln -s /usr/bin/python3.11 /usr/local/bin/python \
    && python -m pip install --no-cache-dir --upgrade pip \
    && python -m pip install --no-cache-dir -r requirements.txt

WORKDIR /usr/local/src/flask_toy/app
# CMD ["uvicorn", "app_fastapi:app", "--host", "0.0.0.0", "--port", "5000"]

# 对Python版本有特定要求， where is the built-in python on ubuntu
# create a softlink for pythonln -s <目标> <链接名称>
# 在Ubuntu中，自带的Python通常位于/usr/bin/python。这是一个符号链接，指向系统默认的Python版本