from fastapi import FastAPI, Request
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.wsgi import WSGIMiddleware
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.openapi.utils import get_openapi
from redis import asyncio as aioredis
import uvicorn

from topics import comment, remoteok
from more_than_http import ws
from app_flask import app as flask_app

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(comment.router)
app.include_router(remoteok.router)
app.include_router(ws.router)
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")
app.mount("/v1", WSGIMiddleware(flask_app))


openapi_schema = get_openapi(
        title="fastapi app",
        version="1.0.0",
        description="This is a very custom OpenAPI schema",
        routes=app.routes,
    )
app.openapi_schema = openapi_schema


@app.on_event("startup")
async def startup_event():
    '''
    connect to redis
    1. https://aioredis.readthedocs.io/en/latest/getting-started/
    2. https://github.com/redis/node-redis/issues/1591#issuecomment-812672883

    url format `redis://arbitrary_usrname:password@ipaddress:6379/0`
    '''
    redis_pw = 'eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81'
    ip = 'cache'
    redis = await aioredis.from_url(f"redis://default:{redis_pw}@{ip}:6379/0")
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")
    print('cache init 🐳')


@app.on_event("shutdown")
def shutdown_event():
    print("Application shutdown 🐬")


@app.get("/", response_class=HTMLResponse)
async def get_html_with_websocket(request: Request):
    '''
    此页面应用了websocket
    '''
    return templates.TemplateResponse("ws.html", {"request": request})


if __name__ == "__main__":
    # http://www.uvicorn.org/#why-asgi
    # https://www.starlette.io/websockets/
    # The IP 0.0.0.0 is commonly used to mean that the program listens on all the IPs available in that machine/server.
    uvicorn.run('app_fastapi:app', host='0.0.0.0', port=5000, reload=True)