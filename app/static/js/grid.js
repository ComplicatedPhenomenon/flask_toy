function gridData() {
    var data = new Array();
    var xpos = 10; //starting xpos and ypos at 1 so the stroke will show when we make the grid below
    var ypos = 10;
    var width = 50;
    var height = 50;
    var click = 0;
    
    // iterate for rows	
    for (var row = 0; row < 10; row++) {
      data.push( new Array() );
      
      // iterate for cells/columns inside rows
      for (var column = 0; column < 10; column++) {
        data[row].push({
          x: xpos,
          y: ypos,
          width: width,
          height: height,
          click: click
        })
        // increment the x position. I.e. move it over by 50 (width variable)
        xpos += width;
      }
      // reset the x position after a row is complete
      xpos = 10;
      // increment the y position for the next row. Move it down 50 (height variable)
      ypos += height;	
    }
    return data;
}

var gridData = gridData();	
// I like to log the data to the console for quick debugging
console.log(gridData);

var grid = d3.select("#grid")
.append("svg")
.attr("width","510px")
.attr("height","510px");

var row = grid.selectAll(".row")
.data(gridData)
.enter().append("g")
.attr("class", "row");

var column = row.selectAll(".square")
.data(d=>d)
.enter().append("rect")
.attr("class","square")
.attr("x", d=>d.x)
.attr("y", d=>d.y)
.attr("width", d=>d.width)
.attr("height", d=>d.height)
.style("fill", "#fff")
.style("stroke", "#222")
.on('click', function(d) {
    d.click ++;
    if ((d.click)%2 == 0 ) { d3.select(this).style("fill","#fff"); }
    if ((d.click)%2 == 1 ) { d3.select(this).style("fill","#2C93E8"); }
    });


/*逻辑上的照猫画虎，以为实现不出来了*/
row.selectAll(".text")
   .data(d=>d)
   .enter().append("text")
   .attr("x", d=>d.x)
   .attr("y", d=>d.y+8)
   .attr("dy", ".35em")
   .style("fill", "#000")
   .text("YLF");


row.selectAll(".text")
   .data(d=>d)
   .enter().append("text")
   .attr("x", d=>d.x+28)
   .attr("y", d=>d.y+42)
   .attr("dy", ".35em")
   .style("fill", "#c00")
   .text("YLF");