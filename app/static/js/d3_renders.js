function construct_stable_part(svg, margin, label){
    var width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom,
    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleTime().rangeRound([0, width]);
    var y = d3.scaleLinear().rangeRound([height, 0]);
    const xAxis = d3.axisBottom(x).tickSize(-height).tickFormat(
        function(date){
            if (d3.timeYear(date) < date) {
                return d3.timeFormat('%H:%M')(date);
            } else {
                return d3.timeFormat('%Y')(date);
            }
        }
    );

    const yAxis = d3.axisLeft(y).tickSize(-width).ticks(15, "s");
    g.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .attr("class", "xaxis")

    g.append("g")
        .append("text")
        .attr("class", "d3_axis_label")
        .attr("x", width/2)
        .attr("y", height + 16)
        .attr("dy", "0.71em")
        .attr("text-anchor", "middle")
        .text(label.x_label);

    const yAxisG = g.append("g")
                    .call(yAxis)
                    .attr("class", "yaxis");

    yAxisG.selectAll('.tick text')
        .attr('x', '-6');

    yAxisG.append("text")
        .attr("class", "d3_axis_label")
        .attr("transform", "rotate(-90)")
        .attr("x", -height/2)
        .attr("y", -45)
        .attr("dy", "0.71em")
        .attr("text-anchor", "middle")
        .text(label.y_label);

    g.append('text')
        .attr('class', 'd3_title')
        .attr('y', -10)
        .attr('x', width/2)
        .attr("text-anchor", "middle")
        .text(label.title);

    return {x:x, y:y, g:g, width:width, height:height}
}

var bisectDate = d3.bisector((d) => { return d.date; }).left;

// https://stackoverflow.com/questions/15471224/how-to-format-time-on-xaxis-use-d3-js
var formatMillisecond = d3.timeFormat(".%L"),
    formatSecond = d3.timeFormat(":%S"),
    formatMinute = d3.timeFormat("%I:%M"),
    formatHour = d3.timeFormat("%I %p"),
    formatDay = d3.timeFormat("%a %d"),
    formatWeek = d3.timeFormat("%b %d"),
    formatMonth = d3.timeFormat("%B"),
    formatYear = d3.timeFormat("%Y");

function multiFormat(date) {
  return (d3.timeSecond(date) < date ? formatMillisecond
      : d3.timeMinute(date) < date ? formatSecond
      : d3.timeHour(date) < date ? formatMinute
      : d3.timeDay(date) < date ? formatHour
      : d3.timeMonth(date) < date ? (d3.timeWeek(date) < date ? formatDay : formatWeek)
      : d3.timeYear(date) < date ? formatMonth
      : formatYear)(date);
}

// 与数据相关的部分，一张图表里应该变化的部分
function construct_vary_part(width, height, x, y, g, dataS_ls){
    // https://github.com/d3/d3-scale/blob/main/src/continuous.js
    x.domain(d3.extent(dataS_ls.flat(), function (d) { return d.date; }));
    var domain_range = d3.extent(dataS_ls.flat(), function (d) { return d.value; });
    y.domain(domain_range);

    var xaxis = d3.axisBottom(x).tickSize(-height).tickFormat(d=>multiFormat(d));
    g.selectAll("g.xaxis")
        .transition().duration(1000)
        .call(xaxis);

    g.selectAll("g.yaxis")
        .transition().duration(1000)
        .call(d3.axisLeft(y).tickSize(-width).ticks(15, "s"));

    if (dataS_ls.length == 2){
        console.log("🚣", dataS_ls[0][0], dataS_ls[1][0]);
    }

    var myColor = d3.schemeCategory10;

    for (var ix = 0; ix < dataS_ls.length; ix++){
        var dataS = dataS_ls[ix];
        var line = d3.line()
            .x(d=> x(d.date))
            .y(d=> y(d.value));
        g.append("path")
            .datum(dataS)
            .attr("fill", "none")
            .attr("stroke", myColor[ix])
            .attr("stroke-linejoin", "round")
            .attr("stroke-linecap", "round")
            .attr("stroke-width", 1)
            .attr("opacity", 0.5)
            .attr("d", line);
    }
    // https://github.com/d3/d3-selection/blob/v3.0.0/README.md#joining-data

    var mouseG = g
    .attr("class", "mouse-over-effects");

    mouseG.append("path") // this is the black vertical line to follow mouse
        .attr("class", "mouse-line")
        .style("stroke", "black")
        .style("stroke-width", "1px")
        .style("opacity", "0");

    // 这样一个comparator 来决定一个数据它被放置进去后的位置

    var focus = g.append("g")
    .attr("class", "focus")
    .style("display", "none");

    focus.append("circle")
    .attr("r", 2.5)
    .attr("fill", "none")
    .attr("stroke", 'green');;

    focus.append("text")
    .attr("x", 9)
    .attr("dy", ".35em");

    g.append("rect")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", mouseover)
        .on("mouseout", mouseout)
        .on("mousemove", mousemove);

    function mousemove(event) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this
        var x0 = x.invert(d3.pointer(event)[0]),
            i = bisectDate(dataS_ls[0], x0, 1),
            d0 = dataS_ls[0][i-1],
            d1 = dataS_ls[0][i];
        // console.log("x0: ", x0, i);
        // console.log("🐼", d3.bisectRight(dataS, x0))
        if (d1!=null){
            // 鼠标指的这个点x0 究竟离它左右两个数据点那个更近
            ix = x0 - d0.date > d1.date - x0 ? i-1 : i;
            console.log('ix', ix);
            var coords = d3.pointer(event);
            console.log("coords:", coords);
            console.log("y: ", dataS_ls[0][ix].value)
            var ratio = height / (domain_range[1]-domain_range[0]);
            var yloc = (domain_range[1] - dataS_ls[0][ix].value)*ratio;
            console.log(x(dataS_ls[0][ix].date) + "," + y(dataS_ls[0][ix].value))
            console.log(x(dataS_ls[0][ix].date) + "," + yloc)
            focus.attr("transform", "translate(" + x(dataS_ls[0][ix].date) + "," + yloc + ")");
            // d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            // focus.attr("transsform", "translate(" + x(d.date) + "," + y(d.value) + ")");
            focus.select("text")
                .text(dataS_ls[0][ix].date.toString() + ', ' + dataS_ls[0][ix].value.toString())
                // .text(d.date.toString() + ', ' + d.value)
                .attr('fill', 'green');

            g.select(".mouse-line")
                .attr("d", function() {
                    var coords = d3.pointer(event); // https://github.com/d3/d3-selection/blob/v3.0.0/README.md#pointer
                    var d = "M" + coords[0] + "," + height;
                    d += " " + coords[0] + "," + 0;
                    return d;
                });
        }
    }


    function mouseover(event){
        g.select(".mouse-line")
          .style("opacity", "1");
        focus.style("display", null);

    }

    function mouseout(){
        g.select(".mouse-line")
        .style("opacity", "0");
        focus.style("display", "none");
    }
}


var line_render = function (data, svg, margin, label) {
    console.log('d3 version:', d3.version);
    var legend = construct_stable_part(svg, margin, label);
    // 为什么直接把legend 当成输入的话会出现 legend.x  得出的x undefined
    var x = legend.x, y = legend.y, g = legend.g, width = legend.width, height = legend.height;
    construct_vary_part(width, height, x, y, g, data);
}


var stacked_render = function(svg, margin, data, groups, subgroups) {
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;
    g = svg.append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleBand()
            .domain(groups)
            .range([0, width])
            .padding([0.2]);

    var y = d3.scaleLinear()
            .domain([0, 1500000000])
            .range([height, 0]);
    // https://github.com/d3/d3-format/blob/v3.1.0/README.md#format
    var yAxisTickFormat = number=>d3.format(".2s")(number).replace("G", "B");
    g.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x).tickSizeOuter(0))
    .selectAll("text")
    .attr("transform", "translate(-10,10)rotate(-45)")
    .style("text-anchor", "end")
    .style("font-size", 12);

    g.append("text")
    .attr("class", "d3_axis_label")
    .attr("x", width/2 - 20)
    .attr("y", height + 80)
    .text("country");

    g.append("g")
    .call(d3.axisLeft(y).tickFormat(yAxisTickFormat));

    g.append("text")
    .attr("class", "d3_axis_label")
    .attr("transform", "rotate(-90)")
    .attr("x",-height/3)
    .attr("y", -45)
    .text("population");

    g.append('text')
    .attr('class', 'd3_title')
    .attr('y', -20)
    .attr('x', width/5)
    .text('Religion by country Top20');

    var color = d3.scaleOrdinal().domain(subgroups)
                .range(d3.schemePaired);

    var stackedData = d3.stack().keys(subgroups)(data);

    var mouseover = function(d) {
        var subgroupName = d3.select(this.parentNode).datum().key;
        d3.selectAll("#stacked_chart .myRect, #stacked_chart .legend").style("opacity", 0.2);
        d3.selectAll("."+subgroupName + ", .legend" + subgroupName)
          .style("opacity", 1)
        };

    var mouseleave = function(d) {
        d3.selectAll("#stacked_chart .myRect, #stacked_chart .legend")
        .style("opacity",0.8)
    };

    // selectAll-then-bind-then-append https://stackoverflow.com/questions/49212955
    // when-using-d3-js-why-must-i-call-selectall-after-select-when-appending-new-eleme
    g.append("g")
      .selectAll("g")
      .data(stackedData)
      .enter() // 数据没有绑定元素 https://github.com/d3/d3-selection/blob/v3.0.0/README.md#selection_enter
      .append("g")
      .attr("fill", d=>color(d.key))
      .attr("class", d=> "myRect " + d.key)
      .selectAll("rect")
      .data(d=>d)
      .enter()
      .append("rect")
      .attr("x", d=>x(d.data.country))
      .attr("y", d=>y(d[1]))
      .attr("height", d=>(y(d[0]) - y(d[1])))
      .attr("width", x.bandwidth())
      .attr("stroke", "green")
      .on("mouseover", mouseover)
      .on("mouseleave", mouseleave);

    var legend = g.append('g')
                .attr('transform', 'translate(' + (width - 150) + ','+ 30  + ')');

    legend.selectAll('rect')
        .data(stackedData)
        .enter()
        .append('rect')
        .attr("class", d=> "legend " + d.key)
        .attr('x', 0)
        .attr('y', (d,i)=> i * 18)
        .attr('width', 12)
        .attr('height', 12)
        .attr('fill', (_,i)=>color(i))
        .attr('stroke', 'green');

    // 图例的信息
    legend.selectAll('text')
        .data(stackedData)
        .enter()
        .append('text')
        .text(d=>d.key)
        .attr('x', 15)
        .attr('y', (d,i)=> i * 18)
        .attr('text-anchor', 'start')
        .attr('alignment-baseline', 'hanging');
}


//https://github.com/petrjahoda/medium_d3
var calendar_render = (data, title, svg, width,  height, cellSize) => {
    // Create chart dimensions and scales
    var g = svg.append("g").attr("transform", "translate(" + ((width - cellSize * 53) / 2) + ","
                                    + (height - cellSize * 7) + ")");

    var result = new Map(data.map(d => [d["date"], d["value"]]));
    const color = d3.scaleQuantize()
        .domain(d3.extent(data, d=>d.value))
        .range(["#f3f6e7", "#e7eecf", "#dbe5b7", "#d0dd9f", "#c4d587", "#b8cd6f", "#acc457", "#a1bc3f", "#94b327", "#89ab0f"]);

    // draw the draw the data
    g.append("g")
        .attr("fill", "none")
        .attr("stroke", "#000")
        .attr("stroke-width", "0.1px")
        .selectAll("rect")
        .data(d => d3.timeDays(new Date(d, 0, 1), new Date(d + 1, 0, 1)))
        .enter().append("rect")
        .attr("width", cellSize)
        .attr("height", cellSize)
        .attr("x", d => d3.timeMonday.count(d3.timeYear(d), d) * cellSize)
        .attr("y", d => d.getUTCDay() * cellSize)
        .datum(d3.timeFormat("%Y-%m-%d"))
        .attr('fill', d => {
            return color(result.get(d));})
        // .on("mouseover", function () {
        //     d3.select(this).attr('stroke-width', "2px").attr("fill", "red");
        // })
        .on("mouseout", function () {
            d3.select(this).attr('stroke-width', "0.1px");
        })
        .append("title")
        .text(d => d  + ": " + result.get(d))


    g.append('text')
        .attr('class', 'd3_title')
        .attr('y', -10)
        .attr('x', width/2)
        .attr("text-anchor", "middle")
        .text(title);

}