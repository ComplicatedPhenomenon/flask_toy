import calendar
import json

from flask import Flask, render_template, request, jsonify
from flask_caching import Cache
# from flask_openapi3 import Info, Tag
# from flask_openapi3 import OpenAPI
import pandas as pd
import requests

from config.db_config import DevelopmentConfig
from utility.utils import timeConsumed, get_sql_statements
from utility.raw_data_processing import statistic_on_participants
from utility.init_postgresql import create_db
from model import db, Movie, Topic


ip = 'elasticsearch'

def add_author(x:str):
    return x + ' - ' + 'Hayao Miyazaki'


def split(x: str):
    return x.split('-')


def register_extensions(app):
    """Register Flask extensions."""
    db.init_app(app)


def create_app():
    app = Flask(__name__)
    # info = Info(title="book API", version="1.0.0")
    # app = OpenAPI(__name__, info=info)
    app.jinja_env.filters["add_author"] = add_author
    app.jinja_env.filters["split"] = split
    app.config.from_object(DevelopmentConfig)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # 关闭对模型修改的监控
    app.config['JSON_AS_ASCII'] = False
    register_extensions(app)
    return app


app = create_app()
cache = Cache(app)  # Initialize Cache
# https://stackoverflow.com/questions/34122949/working-outside-of-application-context-flask
# with app.app_context():
#     create_db(db, Movie, Topic)


@app.get('/db_test')
def test_postgresql_json_filter():
    db.session.query(Topic.comments)
    sql_ls = get_sql_statements()
    x = db.engine.execute(sql_ls[0]).fetchall()
    return jsonify(str(x))


@app.route('/get-json', methods=['GET', 'POST'])
def get_json():
    with open('./data/json/miserables.json') as f:
        json_to = json.load(f)
    return jsonify(json_to)


@app.post('/user')
def user_data():
    name1 = eval(request.data.decode())['name1']
    name2 = eval(request.data.decode())['name2']
    data = cache.get(f'user_{name1}_{name2}')
    if data:
        return jsonify(data)
    data = Topic.query.filter(Topic.author_name==name1).first().comments
    df1 = pd.DataFrame(data).sort_values('reply_time')
    sp = statistic_on_participants(df1, name2)
    df = sp.filter_data()
    xdf = df[df.name==name2].iloc[:, 1:]
    xdf.timestamp = pd.to_datetime(xdf.timestamp)
    ps = xdf.groupby(xdf.timestamp.dt.date).value.apply(list).map(sum)
    df2 = ps.reset_index()
    df2.timestamp = df2.timestamp.astype(str)
    data = [dict(zip(['date', 'value'], i)) for i in df2.values]
    cache.set(f'user_{name1}_{name2}', data)
    return jsonify(data)


@app.post('/user_expansion')
def user_expansion2():
    name = eval(request.data.decode())['name']
    names = cache.get(f'user_expansion_{name}')
    if names:
        return jsonify(names)
    data = Topic.query.filter(Topic.author_name==name).first().comments
    df1 = pd.DataFrame(data).sort_values('reply_time')
    sp = statistic_on_participants(df1, name)
    df = sp.filter_data()
    names = df.groupby('name').size().sort_values().index.tolist()
    cache.set(f'user_expansion_{name}', names)
    return jsonify(names)


@app.post('/group_topic')
def group_topic():
    name = eval(request.data.decode())['name']
    data = cache.get(f'group_topic_{name}')
    if data:
        return jsonify(data)
    data = Topic.query.filter(Topic.author_name==name).first().comments
    df1 = pd.DataFrame(data).sort_values('reply_time')
    sp = statistic_on_participants(df1, name)
    df = sp.filter_data()
    xdf = df.iloc[:, 1:]
    xdf.timestamp = pd.to_datetime(xdf.timestamp)
    ps = xdf.groupby(xdf.timestamp.dt.date).value.apply(list).map(sum)
    df2 = ps.reset_index()
    df2.timestamp = df2.timestamp.astype(str)
    data = [dict(zip(['date', 'value'], i)) for i in df2.values]
    cache.set(f'group_topic_{name}', data)
    return jsonify(data)


@app.get('/')
def hello():
    df = pd.read_csv('./data/csv/religionByCountryTop20.csv')
    df2 = df.pivot(index='country', columns='religion')['population']
    df2.columns = ['_'.join(k.split(' ')) for k in df2.columns]
    ids = df2.sum(axis=1).sort_values(ascending=False).index
    groups = json.dumps(list(ids))
    subgroups = json.dumps(list(df2.columns))
    # 制造方便调用d3.stack()的数据结构
    vals = df2.reset_index().values
    cols = df2.reset_index().columns
    data = [dict(zip(cols, val)) for val in vals]
    data = json.dumps(data)
    return render_template('d3_stacked_bar.html', data=data, groups = groups, subgroups = subgroups)


@app.get('/index')
def index():
    movies = []
    name = 'CZFZDXX'
    tem = db.session.query(Movie).all()
    for i in tem:
        movies.append({'title': i.title, 'year': i.year})
    data = calendar.Calendar().yeardayscalendar(year=2021, width = 12)[0]
    return render_template('index.html', name=name, movies=movies, calendar=data)


@app.get('/psutil_info')
def get_psutil_info():
    url = f'http://{ip}:9200/cpu_memory_usage/_search'
    query = {
        "size": 5000,
        "query": {"match_all": {}},
        "sort" : [{"datetime.keyword" : {"order" : "desc"}}]
    }
    headers = {'Content-Type': 'application/json'}
    resp = requests.get(url, headers=headers, data=json.dumps(query))
    data = [item['_source'] for item in resp.json()['hits']['hits']][::-1]
    return jsonify(data)


@app.get('/vis1')
def vis1():
    print('request method:', request.method)
    df = pd.read_csv('./data/csv/stock.csv', sep='\t')
    df.date = pd.to_datetime(df.date).astype(str)
    data = [dict(zip(['date', 'value'], i)) for i in df.values]
    return render_template('vis1.html', data = json.dumps(data))


@app.get('/vis2')
def vis2():
    return render_template('vis2.html')


@app.get('/vis3')
def vis3():
    return render_template('vis3.html')


@app.get('/heatmap')
@timeConsumed
def calendar_heatmap():
    sql = '''
            select distinct author_name from topic;
          '''
    zed = db.engine.execute(sql).fetchall()
    names = [i[0] for i in zed]
    return render_template('calendar_heatmap.html', names0 = names)


@app.get("/vue")
def get():
    '''
    此页面应用了vue
    '''
    return render_template("vue_app.html", message = "🥝 jinja variable")


@app.get("/stream")
def get_data_from_mq_data():
    '''
    此页面用了redis作为消息队列
    '''
    return render_template("stream.html")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)