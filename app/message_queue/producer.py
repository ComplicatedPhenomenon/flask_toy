"""
It sends a python dict (producer, some_id, count)
to REDIS STREAM (using the xadd method)

Usage:
  MESSAGES=20 PRODUCER=WM python3 producer.py 
"""
from os import environ
from redis import Redis
from uuid import uuid4
from time import sleep

stream_key = environ.get("STREAM", "jarless-1")
producer = environ.get("PRODUCER", "user-1")
MAX_MESSAGES = int(environ.get("MESSAGES", "2"))


def send_data(redis_connection, max_messages):
    """
    1. https://redis.readthedocs.io/en/stable/commands.html?highlight=xadd#core-commands
    2. https://redis.readthedocs.io/en/stable/commands.html?highlight=xadd#redis.commands.cluster.RedisClusterCommands.xadd
    3. https://redis.io/commands/xadd/
    4. https://stackoverflow.com/a/67526831/7583919s
    """
    # print(redis_connection.xinfo_stream(stream_key))
    # print(redis_connection.xrange(stream_key, count=3))
    # print(redis_connection.xtrim(stream_key, maxlen=10, approximate=False))
    count = 0
    while count < max_messages:
        try:
            data = {
                "producer": producer,
                "some_id": uuid4().hex,  # Just some random data
                "count": count,
            }
            resp = redis_connection.xadd(stream_key, data, maxlen=10)
            print(resp)
            count += 1

        except ConnectionError as e:
            print("ERROR REDIS CONNECTION: {}".format(e))
        sleep(0.5)


if __name__ == "__main__":
    connection = Redis("cache", 6379, retry_on_timeout=True, password='eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81')
    send_data(connection, MAX_MESSAGES)