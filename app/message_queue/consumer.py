"""
It reads the REDIS STREAM events
Using the xread, it gets 1 event per time (from the oldest to the last one)
"""
import asyncio
from os import environ
from redis import Redis

stream_key = environ.get("STREAM", "jarless-1")


def get_data(redis_connection):
    print(type(redis_connection))
    sleep_ms = 5000
    last_id = 0
    while True:
        try:
            # block怎么起作用
            resp = redis_connection.xread({stream_key: last_id}, count=1, block=sleep_ms)
            # [[b'jarless-1', [(b'1663253687838-0', {b'producer': b'Roger', b'some_id': b'f6ffd48f3ce34877a124f0ce43d4acdb', b'count': b'0'})]]]
            if resp:
                _, messages = resp[0]
                last_id, data = messages[0]
                print("REDIS ID: ", last_id)
                print("      --> ", data)

        except ConnectionError as e:
            print("ERROR REDIS CONNECTION: {}".format(e))


if __name__ == "__main__":
    connection = Redis("cache", 6379, retry_on_timeout=True, password='eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81')
    get_data(connection)