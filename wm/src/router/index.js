import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
const routes = [
  {
    path: '/home',
    name: 'HomeView',
    component: HomeView
  },
  {
    path: '/td',
    name: 'TopicDisplay',
    component: () => import('../views/TableDisplay.vue')
  },
  {
    path: '/tc/:topic_id',
    name: 'TopicComments',
    component: () => import('../views/TopicComments.vue')
  },
  {
    path: '/ts',
    name: 'TopicSummary',
    component: () => import('../views/TopicSummary.vue')
  },
  {
    path: '/tc/job_list',
    name: 'JobList',
    component: () => import('../views/JobList.vue')
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
})

export default router
